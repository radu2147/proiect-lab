package socialnetwork.service.exceptions;

public class NoUserLoggedIn extends RuntimeException{
    public NoUserLoggedIn(String s){
        super(s);
    }
}
