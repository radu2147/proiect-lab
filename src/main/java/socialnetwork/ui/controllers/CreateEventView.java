package socialnetwork.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.LocalDateTime;

public class CreateEventView extends Controller {

    @FXML
    Button button;

    @FXML
    TextField title;

    @FXML
    TextField desc;

    @FXML
    DatePicker date;

    @Override
    public void initialize() {
        button.setOnMouseClicked(e -> {
            try {
                String[] args = date.getEditor().getText().split("/");
                LocalDateTime datetime = LocalDateTime.of(Integer.parseInt(args[2]), Integer.parseInt(args[0]), Integer.parseInt(args[1]), 0, 0);
                page.insertSocialEvent(title.getText(), desc.getText(), datetime);
            }
            catch(Exception ee){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText(ee.toString());
                a.show();
            }
            ((Stage) button.getScene().getWindow()).close();
        });
    }

    @Override
    public void observe() {

    }
}
