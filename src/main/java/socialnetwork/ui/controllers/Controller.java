package socialnetwork.ui.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.observers.Observer;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.Page;
import socialnetwork.service.UtilizatorService;

public abstract class Controller implements Observer {
    protected UtilizatorService service;
    protected FriendshipService fr_serv;
    protected Page page;

    public void switch_view(String string, Node name){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(string));
            Stage stage = (Stage) name.getScene().getWindow();
            AnchorPane pane = loader.load();
            Controller controller = loader.getController();
            controller.setPage(page);
            controller.initialize();
            change_controller(controller);
            stage.setScene(new Scene(pane, 600, 650));
            stage.setTitle("Piep Piper");
        }
        catch (Exception ec){
            ec.printStackTrace();
        }
    }

    public void change_controller(Controller c){
        page.getUserService().setObservers(this, c);
    }

    public void setPage(Page page){
        this.page = page;
    }

    public abstract void initialize();


    public void setService(UtilizatorService userServ){
        this.service = userServ;
    }
    public void setService(FriendshipService userServ){
        this.fr_serv = userServ;
    }
}
