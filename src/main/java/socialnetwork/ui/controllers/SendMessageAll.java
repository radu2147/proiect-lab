package socialnetwork.ui.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.Utils;
import socialnetwork.domain.Message;
import socialnetwork.domain.UserDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SendMessageAll extends Controller{

    private ObservableList<UserDTO> list = FXCollections.observableArrayList();

    public ListView<UserDTO> contacts;
    public TextField nameSearch;
    public Button messageButton;

    private void uiSendMessage() {
        ArrayList<Long> arr = new ArrayList<>();
        List<Message> rez;
        for(UserDTO el: contacts.getSelectionModel().getSelectedItems())
            arr.add(el.getId());
        rez = page.send_message(nameSearch.getText(), arr, LocalDateTime.now());
        for(UserDTO el: contacts.getSelectionModel().getSelectedItems()) {
            List<Message> list = Utils.iterablToList(page.getMessages().get(el.getId()));
            list.add(rez.get(0));
            page.getMessages().put(el.getId(), list);
        }
    }


    @Override
    public void initialize() {
        if(page != null){
            contacts.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            list.setAll(Utils.iterablToList(page.getFriends()));
            contacts.setItems(list);
            messageButton.setOnAction(e -> {
                uiSendMessage();
                ((Stage)messageButton.getScene().getWindow()).close();
            });
        }
    }

    @Override
    public void observe() {

    }
}
