package socialnetwork.encryption;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class Crypter {
    public static String encryptPassword(String passwd) {
        char[] rez = BCrypt.withDefaults().hashToChar(6, passwd.toCharArray());
        return String.valueOf(rez);
    }
}
