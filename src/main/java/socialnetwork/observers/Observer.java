package socialnetwork.observers;

public interface Observer {
    void observe();
}
