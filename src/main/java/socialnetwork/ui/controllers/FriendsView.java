package socialnetwork.ui.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.UserDTO;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendsView extends Controller{
    public MenuItem messenger;
    public MenuItem export;
    ObservableList<UserDTO> list = FXCollections.observableArrayList();

    @FXML
    Label name;

    @FXML
    MenuItem home;

    @FXML
    MenuItem profile;

    @FXML
    TextField nameSearch;

    @FXML
    TableView<UserDTO> people;

    @FXML
    TableColumn<UserDTO, String> columnPrenume;

    @FXML
    TableColumn<UserDTO, String> columnNume;

    @FXML
    Button del_friend;

    @FXML
    MenuItem logout;

    @FXML
    MenuItem requests;

    @Override
    public void initialize(){
        columnPrenume.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        columnNume.setCellValueFactory(new PropertyValueFactory<>("last_name"));
        people.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> del_friend.setVisible(people.getSelectionModel().getSelectedItem() != null));
        if(page != null){
            logout.setOnAction(e -> {
                page.logout();
            });
            del_friend.setOnMouseClicked(ep -> {
                try{
                    page.del_prietenie(new Tuple<>(page.getCurrentUser().getId(), people.getSelectionModel().getSelectedItem().getId()));
                    list.setAll(StreamSupport.stream(page.userFriends().spliterator(), false)
                            .filter(e -> (e.getFirst_name() + " " + e.getLast_name()).startsWith(nameSearch.getText()))
                            .collect(Collectors.toList()));
                    people.setItems(list);
                }
                catch(Exception es){
                    es.printStackTrace();
                    Alert a = new Alert(Alert.AlertType.ERROR);
                    a.setContentText(es.toString());
                    a.show();
                }
            });
            list.setAll(StreamSupport.stream(page.userFriends().spliterator(), false)
                    .collect(Collectors.toList()));
            people.setItems(list);
            name.setText(page.getCurrentUser().getFirstName() + " " + page.getCurrentUser().getLastName());
            nameSearch.textProperty().addListener((observable, oldValue, newValue) -> {
                list.setAll(StreamSupport.stream(page.userFriends().spliterator(), false)
                        .filter(e -> (e.getFirst_name() + " " + e.getLast_name()).startsWith(nameSearch.getText()))
                        .collect(Collectors.toList()));
                people.setItems(list);
            });
        }
        home.setOnAction(e -> switch_view("/views/user_view.fxml", name));
        requests.setOnAction(e -> switch_view("/views/requests_view.fxml", name));
        profile.setOnAction(e -> switch_view("/views/profile_view.fxml", name));
        messenger.setOnAction(e -> switch_view("/views/messenger_view.fxml", name));
    }

    @Override
    public void observe() {

    }
}
