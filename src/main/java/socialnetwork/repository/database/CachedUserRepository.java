package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CachedUserRepository extends AbstractDbRepository<Long, Utilizator> {
    public CachedUserRepository(String url, String username, String password, Validator<Utilizator> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected Utilizator mapDbObject(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String username = resultSet.getString("username");
        String password = resultSet.getString("password");
        boolean notifications = resultSet.getBoolean("notifications");

        return new Utilizator(id, firstName, lastName, username, password, notifications);
    }

    @Override
    public Utilizator save(Utilizator entity) {
        String sqlQuery = "insert into logged_user(id, first_name, last_name, username, password, notifications) values(" +entity.getId() + ",'" + entity.getFirstName() + "','" + entity.getLastName() +"','" + entity.getUsername() + "','" + entity.getPassword() +"'," + entity.isNotifications() + ");";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public Utilizator delete(Long aLong) {
        String sqlQuery = "delete from logged_user where id=" + aLong;
        setQuery(sqlQuery);
        return super.delete(aLong);
    }

    @Override
    public Utilizator findOne(Long aLong) {
        String sqlQuerry = "SELECT * from logged_user where id=" + aLong;
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<Utilizator> findAll() {
        String sqlQuerry = "SELECT * from logged_user;";
        setQuery(sqlQuerry);
        return super.findAll();
    }
}
