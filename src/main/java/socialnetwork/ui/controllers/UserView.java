package socialnetwork.ui.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import socialnetwork.domain.Status;
import socialnetwork.domain.UserDTO;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UserView extends Controller{

    ObservableList<UserDTO> list = FXCollections.observableArrayList();

    @FXML
    Button accept;

    @FXML
    Button reject;

    @FXML
    HBox toolbar;

    @FXML
    MenuItem logout;

    @FXML
    MenuItem profile;

    @FXML
    MenuItem messenger;

    @FXML
    TextField nameSearch;

    @FXML
    TableView<UserDTO> people;

    @FXML
    TableColumn<UserDTO, String> columnPrenume;

    @FXML
    TableColumn<UserDTO, String> columnNume;

    @FXML
    VBox buttons;

    @FXML
    Label name;

    @FXML
    Button send_request;

    @FXML
    HBox response;

    @FXML
    MenuItem friends;

    @FXML
    MenuItem requests;

    @FXML
    TableColumn<UserDTO, String> columnStatus;




    @Override
    public void initialize(){
        columnPrenume.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        columnNume.setCellValueFactory(new PropertyValueFactory<>("last_name"));

        toolbar.setAlignment(Pos.CENTER);
        if(page != null) {
            if (!page.anonymousSession()) {
                reloadTableView();
                name.setText(page.getCurrentUser().getFirstName() + " " + page.getCurrentUser().getLastName());
                logout.setOnAction(event -> {
                    page.logout();
                });
                nameSearch.textProperty().addListener((observable, oldValue, newValue) -> {
                    reloadTableView();
                });
            }
        }
        if(page != null){
            columnStatus.setCellValueFactory(param -> new SimpleStringProperty(page.areFriends(param.getValue().getId()) ? "FRIENDS" : ""));

            accept.setOnMouseClicked(e -> {
                page.respond_req(people.getSelectionModel().getSelectedItem().getId(), Status.ACCEPTED);
                page.load();
                reloadTableView();
                response.setVisible(false);
                send_request.setText("You are friends");
            });
            reject.setOnMouseClicked(e -> {
                page.respond_req(people.getSelectionModel().getSelectedItem().getId(), Status.REJECTED);
                response.setVisible(false);
                send_request.setText("+ Send Request");
            });
            try {
                people.getSelectionModel().selectedItemProperty().addListener((obzable, oldValue, newValue) -> {
                    buttons.setVisible(people.getSelectionModel().getSelectedItem() != null);
                    if(people.getSelectionModel().getSelectedItem() != null) {
                        if(page.areFriends(people.getSelectionModel().getSelectedItem().getId())){
                            send_request.setText("You are friends");
                            response.setVisible(false);
                        }
                        else if (page.hasSentRequest(people.getSelectionModel().getSelectedItem().getId())) {
                            send_request.setText(":) Request sent");
                            response.setVisible(false);
                        }
                        else if (page.hasReceivedRequest(people.getSelectionModel().getSelectedItem().getId())) {
                            send_request.setText("? Pending");
                            response.setVisible(true);
                        }
                        else {
                            send_request.setText("+ Send request");
                            response.setVisible(false);
                        }
                    }
                });

            }
            catch (Exception ex){
                ex.printStackTrace();
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText(ex.toString());
                a.show();
            }
            send_request.setOnMouseClicked(e -> {
                if(send_request.getText().equals("+ Send request")){
                    send_request.setText(":) Request sent");
                    page.sendFrReq(people.getSelectionModel().getSelectedItem().getId());
                }
                else if(send_request.getText().equals(":) Request sent")){
                    send_request.setText("+ Send request");
                    page.delFrReq(people.getSelectionModel().getSelectedItem().getId());
                }
            });
        }
        profile.setOnAction(e -> switch_view("/views/profile_view.fxml", name));
        friends.setOnAction(e -> switch_view("/views/friends_view.fxml", name));
        requests.setOnAction(e -> switch_view("/views/requests_view.fxml", name));
        messenger.setOnAction(e -> switch_view("/views/messenger_view.fxml", name));

    }

    private void reloadTableView() {
        list.setAll(StreamSupport.stream(page.getAll().spliterator(), false)
                .filter(e -> e.display_name().startsWith(nameSearch.getText()) && !e.display_name().equals(page.getCurrentUser().display_name()))
                .map(e -> new UserDTO(e.getId(), e.getFirstName(), e.getLastName(), e.getUsername()))
                .collect(Collectors.toList()));
        people.setItems(list);
    }

    @Override
    public void observe() {

    }
}