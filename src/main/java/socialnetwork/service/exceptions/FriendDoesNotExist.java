package socialnetwork.service.exceptions;

/**
 * Exception class thrown when trying to delete of update an existing Prietenie
 */
public class FriendDoesNotExist extends RuntimeException{
    public FriendDoesNotExist(){

    }
    public FriendDoesNotExist(String message){
        super(message);
    }
}
