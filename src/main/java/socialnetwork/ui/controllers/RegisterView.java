package socialnetwork.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.Page;
import socialnetwork.service.UtilizatorService;

public class RegisterView {

    private Page page;

    private Stage stage;

    @FXML
    TextField first_name;
    @FXML
    TextField second_name;
    @FXML
    Button register;
    @FXML
    TextField username;
    @FXML
    PasswordField password;

    @FXML
    VBox register_layout;

    @FXML
    Label login_redirect;

    @FXML
    Label error;

    public void initialize(){

        login_redirect.setOnMouseClicked(e -> {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/login_view.fxml"));
                AnchorPane pane = loader.load();
                LoginView reg = loader.getController();
                reg.setPage(page);

                stage.setScene(new Scene(pane, 450, 300));
            }
            catch(Exception es){

            }
        });

        register_layout.setAlignment(Pos.CENTER);
        register.setOnMouseClicked(e -> {
            try {
                page.addUtilizator(new Utilizator(first_name.getText(), second_name.getText(), username.getText(), password.getText(), true));
                page.login_user(username.getText(), password.getText());

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/user_view.fxml"));
                AnchorPane pane = loader.load();
                UserView controller = loader.getController();
                page.load();
                controller.setPage(page);
                controller.initialize();
                stage.setScene(new Scene(pane, 600, 650));
            } catch (Exception es) {
                error.setVisible(true);
                es.printStackTrace();
            }
        });
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
