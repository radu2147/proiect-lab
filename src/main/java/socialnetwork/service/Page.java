package socialnetwork.service;

import socialnetwork.domain.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Page {
    private Utilizator user;
    private HashMap<Long,Iterable<Message>> messages;
    private Iterable<FriendRequest> reqs;
    private Iterable<UserDTO> friends;
    private UtilizatorService userService;
    private FriendshipService friendshipService;
    private Iterable<SocialEvent> events;
    private Iterable<SocialEvent> todayEvents;

    public Page(Utilizator user, HashMap<Long, Iterable<Message>> messages, Iterable<FriendRequest> reqs, Iterable<UserDTO> friends, Iterable<SocialEvent> events, Iterable<SocialEvent> todayEvents){
        this.user = user;
        this.messages = messages;
        this.reqs = reqs;
        this.friends = friends;
        this.events = events;
        this.todayEvents = todayEvents;
    }

    public Page(){

    }

    public Page(UtilizatorService userService, FriendshipService friendshipService){

        this.userService = userService;
        this.friendshipService = friendshipService;
    }

    public boolean anonymousSession(){
        return userService.anonymousSession();
    }

    public void load(){
        friendshipService.loadPage(this);
        userService.loadPage(this);
    }

    public void respond_req(Long id, Status status){
        friendshipService.respond_req(id, status);
    }

    public boolean areFriends(Long id){
        return friendshipService.areFriends(id);
    }

    public boolean hasSentRequest(Long id){
        return friendshipService.hasSentRequest(id);
    }

    public boolean hasReceivedRequest(Long id){
        return friendshipService.hasReceivedRequest(id);
    }

    public void sendFrReq(Long id){
        friendshipService.sendFrReq(id);
    }

    public void logout(){
        userService.logout();
        friendshipService.logout();
    }

    public Utilizator getCurrentUser(){
        return userService.getCurrentUser();
    }

    public Utilizator getUser() {
        return user;
    }

    public void setUser(Utilizator user) {
        this.user = user;
    }

    public HashMap<Long,Iterable<Message>> getMessages() {
        return messages;
    }

    public void setMessages(HashMap<Long,Iterable<Message>> messages) {
        this.messages = messages;
    }

    public Iterable<FriendRequest> getReqs() {
        return reqs;
    }

    public void setReqs(Iterable<FriendRequest> reqs) {
        this.reqs = reqs;
    }

    public Iterable<UserDTO> getFriends() {
        return friends;
    }

    public void setFriends(Iterable<UserDTO> friends) {
        this.friends = friends;
    }

    public Iterable<SocialEvent> getEvents() {
        return events;
    }

    public void setEvents(Iterable<SocialEvent> events) {
        this.events = events;
    }

    public Iterable<SocialEvent> getTodayEvents() {
        return todayEvents;
    }

    public void setTodayEvents(Iterable<SocialEvent> todayEvents) {
        this.todayEvents = todayEvents;
    }

    public UtilizatorService getUserService() {
        return userService;
    }

    public void setUserService(UtilizatorService userService) {
        this.userService = userService;
    }

    public FriendshipService getFriendshipService() {
        return friendshipService;
    }

    public void login_user(String user, String password){
        friendshipService.login_user(user, password);
        userService.login_user(user, password);
    }

    public void setFriendshipService(FriendshipService friendshipService) {
        this.friendshipService = friendshipService;
    }

    public void fastLogin(Utilizator user){
        friendshipService.fastLogin(user);
        userService.fastLogin(user);
    }

    public void delFrReq(Long id) {
        friendshipService.delFrReq(id);
    }

    public void del_prietenie(Tuple<Long, Long> longLongTuple) {
        friendshipService.del_prietenie(longLongTuple);
    }

    public Iterable<UserDTO> userFriends() {
        return friendshipService.userFriends();
    }

    public void unsubscribe(Long id) {
        userService.unsubscribe(id);
    }

    public void subscribe(Long id) {
        userService.subscribe(id);
    }

    public boolean isSubscribed(Long id) {
        return userService.isSubscribed(id);
    }

    public Iterable<SocialEvent> getEventsPage(int size, int i) {
        return userService.getEventsPage(size, i);
    }

    public List<Message> send_message(String text, ArrayList<Long> arr, LocalDateTime now) {
        return userService.send_message(text, arr, now);
    }

    public List<Message> sendReplyTo(String text, Long id, ArrayList<Long> arr, LocalDateTime now) {
        return userService.sendReplyTo(text, id, arr, now);
    }

    public Iterable<Message> getMessagePage(Long id, int size, int i) {
        return userService.getMessagePage(id, size, i);
    }

    public List<FriendRequest> getRequestOfUser() {
        return friendshipService.getRequestOfUser();
    }

    public Utilizator getFriendRequestEndPoint(FriendRequest selectedItem) {
        return friendshipService.getFriendRequestEndPoint(selectedItem);
    }

    public Iterable<Utilizator> getAll() {
        return userService.getAll();
    }

    public Iterable<Message> getConversation(Long id) {
        return userService.getConversation(id);
    }

    public HashMap<String, Integer> countMessagesPerDate(Long id) {
        return userService.countMessagesPerDate(id);
    }

    public Iterable<Message> filterMessage(Long id, LocalDateTime startDate, LocalDateTime endDate) {
        return userService.filterMessage(id, startDate, endDate);
    }

    public void addUtilizator(Utilizator utilizator) throws Exception {
        if(userService.addUtilizator(utilizator) != null){
            throw new Exception("User already exists");
        }
    }

    public HashMap<String, Integer> countRequestsPerDate() {
        return friendshipService.countRequestsPerDate();
    }

    public Iterable<Message> filterMessagesAll(LocalDateTime startDate, LocalDateTime endDate) {
        return userService.filterMessagesAll(startDate, endDate);
    }

    public Iterable<FriendRequest> userFriendRequestsFiltered(LocalDateTime startDate, LocalDateTime endDate) {
        return friendshipService.userFriendRequestsFiltered(startDate, endDate);
    }

    public void rememberUser(Utilizator user){
        userService.rememberUser(user);
    }

    public Utilizator isUserRemembered(){
        return userService.isUserRemembered();
    }

    public void insertSocialEvent(String text, String text1, LocalDateTime datetime) {
        userService.insertSocialEvent(text, text1, datetime);
    }
}
