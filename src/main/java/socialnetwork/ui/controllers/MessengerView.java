package socialnetwork.ui.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.Utils;
import socialnetwork.domain.Message;
import socialnetwork.domain.ReplyMessage;
import socialnetwork.domain.UserDTO;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessengerView extends Controller {
    public MenuItem logout;
    public Button multiple;
    ObservableList<UserDTO> list = FXCollections.observableArrayList();
    ObservableList<Message> messageList = FXCollections.observableArrayList();

    @FXML
    MenuItem home;

    @FXML
    MenuItem profile;

    @FXML
    MenuItem requests;

    @FXML
    MenuItem friends;

    @FXML
    ListView<Message> messagesListView;

    @FXML
    Button loader;

    @FXML
    Label name;

    @FXML
    TextField nameSearch;

    @FXML
    ListView<UserDTO> contacts;

    @FXML
    Button messageButton;

    @FXML
    Button export;

    @FXML
    MenuItem exportActivities;

    @Override
    public void initialize() {
        contacts.setMaxWidth(150);
        if(page != null){
            nameSearch.setOnKeyPressed(e -> {
                if(e.getCode().equals(KeyCode.ENTER) && contacts.getSelectionModel().getSelectedItem() != null && !nameSearch.getText().equals("")){
                    uiSendMessage();
                }
            });
            logout.setOnAction(e -> page.logout());

            multiple.setOnAction(e -> {
                try{
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/views/send_message_all.fxml"));
                    AnchorPane pane = loader.load();
                    SendMessageAll controller = loader.getController();
                    controller.setPage(page);
                    controller.initialize();
                    change_controller(controller);
                    Stage stage = new Stage();
                    stage.setScene(new Scene(pane, 552, 580));
                    stage.setTitle("Pied Piper");
                    stage.show();
                }
                catch(Exception ex){

                }
            });

            exportActivities.setOnAction(e -> {
                try{
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/views/export_activities_view.fxml"));
                    AnchorPane pane = loader.load();
                    ExportActivitiesView controller = loader.getController();
                    controller.setPage(page);
                    controller.setUser(new UserDTO(page.getCurrentUser().getId(), page.getCurrentUser().getFirstName(), page.getCurrentUser().getLastName(), page.getCurrentUser().getUsername()));
                    controller.initialize();
                    change_controller(controller);
                    Stage stage = new Stage();
                    stage.setScene(new Scene(pane, 552, 620));
                    stage.setTitle("Pied Piper");
                    stage.show();
                }
                catch (Exception ec){
                    ec.printStackTrace();
                }
            });

            export.setOnMouseClicked(e -> {
                try{
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/views/export_view.fxml"));
                    AnchorPane pane = loader.load();
                    ExportView controller = loader.getController();
                    controller.setPage(page);
                    controller.setUser(contacts.getSelectionModel().getSelectedItem());
                    controller.initialize();
                    change_controller(controller);
                    Stage stage = new Stage();
                    stage.setScene(new Scene(pane, 552, 620));
                    stage.setTitle("Pied Piper");
                    stage.show();
                }
                catch (Exception ec){
                    ec.printStackTrace();
                }
            });

            messageButton.setOnMouseClicked(e -> {
                if(contacts.getSelectionModel().getSelectedItem() != null && !nameSearch.getText().equals(""))
                    uiSendMessage();
            });
            messageButton.setMaxWidth(40);
            contacts.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                export.setVisible(contacts.getSelectionModel().getSelectedItem() != null);
                loader.setVisible(contacts.getSelectionModel().getSelectedItem() != null);
                messageList.setAll(Utils.iterablToList(page.getMessages().get(contacts.getSelectionModel().getSelectedItem().getId())));
                messagesListView.setItems(messageList);
                messagesListView.scrollTo(messageList.size() - 1);
            });
            loader.setOnAction(e ->{
                Task<List<Message>> task = new Task<List<Message>>(){

                    @Override
                    protected List<Message> call() throws Exception {
                        List<Message> as = Utils.iterablToList(page.getMessagePage(contacts.getSelectionModel().getSelectedItem().getId(), messageList.size(), 5));
                        return as;
                    }
                };
                task.setOnSucceeded(event -> {
                    List<Message> as = task.getValue();
                    as.addAll(messageList);
                    messageList.setAll(as);
                    page.getMessages().put(contacts.getSelectionModel().getSelectedItem().getId(), as);
                    messagesListView.setItems(messageList);
                    messagesListView.scrollTo(messageList.size() - 15);
                });
                new Thread(task).start();
            });

            messagesListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Message>() {
                @Override
                public void changed(ObservableValue<? extends Message> observable, Message oldValue, Message newValue) {
                    System.out.println(newValue);
                }
            });

            messagesListView.setItems(messageList);
            messagesListView.setCellFactory(param -> new ListCell<Message>(){

                final Label lblTextLeft = new Label();
                final Pane leftPane = new Pane(lblTextLeft);
                final HBox hBoxLeft = new HBox(leftPane);

                final Label lblTextRight = new Label();
                final Pane rightPane = new Pane(lblTextRight);
                final HBox hBoxRight = new HBox(rightPane);
                {
                    lblTextLeft.setStyle("-fx-background-color: #3E4042; -fx-padding: 5px; -fx-text-fill: white; -fx-background-radius: 10px");
                    lblTextRight.setStyle("-fx-background-color: #017ADA; -fx-padding: 5px; -fx-text-fill: white; -fx-background-radius: 10px");

                    hBoxLeft.setAlignment(Pos.CENTER_LEFT);
                    hBoxLeft.setSpacing(5);
                    hBoxRight.setAlignment(Pos.CENTER_RIGHT);
                    hBoxRight.setSpacing(5);
                }
                @Override
                protected void updateItem(Message item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty){
                        setText(null);
                        setGraphic(null);
                    }
                    else{
                        if(!item.getFrom().getId().equals(page.getCurrentUser().getId())){
                            if(item instanceof ReplyMessage)
                                lblTextLeft.setText("Reply To: " + ((ReplyMessage) item).getReplyTo().getText() + "\n" + item.getText());
                            else
                                lblTextLeft.setText(item.getText());
                            setGraphic(hBoxLeft);
                        }
                        else{
                            if(item instanceof ReplyMessage)
                                lblTextRight.setText("Reply To: " + ((ReplyMessage) item).getReplyTo().getText() + "\n" + item.getText());
                            else
                                lblTextRight.setText(item.getText());
                            setGraphic(hBoxRight);
                        }
                    }
                }
            });

            list.setAll(Utils.iterablToList(page.getFriends()));
            contacts.setItems(list);
            name.setText(page.getUser().getFirstName() + " " + page.getUser().getLastName());
        }
        profile.setOnAction(e -> switch_view("/views/profile_view.fxml", name));
        friends.setOnAction(e -> switch_view("/views/friends_view.fxml", name));
        requests.setOnAction(e -> switch_view("/views/requests_view.fxml", name));
        home.setOnAction(e -> switch_view("/views/user_view.fxml", name));
    }

    private void uiSendMessage() {
        ArrayList<Long> arr = new ArrayList<>();
        List<Message> rez;
        arr.add(contacts.getSelectionModel().getSelectedItem().getId());
        if (messagesListView.getSelectionModel().getSelectedItem() == null)
            rez = page.send_message(nameSearch.getText(), arr, LocalDateTime.now());
        else
            rez = page.sendReplyTo(nameSearch.getText(), messagesListView.getSelectionModel().getSelectedItem().getId(), arr, LocalDateTime.now());
        List<Message> list = Utils.iterablToList(page.getMessages().get(contacts.getSelectionModel().getSelectedItem().getId()));
        list.add(rez.get(0));
        page.getMessages().put(contacts.getSelectionModel().getSelectedItem().getId(), list);
        messageList.setAll(Utils.iterablToList(page.getMessages().get(contacts.getSelectionModel().getSelectedItem().getId())));
        nameSearch.setText("");
    }

    @Override
    public void observe() {
        messageList.setAll(Utils.iterablToList(page.getConversation(contacts.getSelectionModel().getSelectedItem().getId())));
        messagesListView.setItems(messageList);
    }
}
