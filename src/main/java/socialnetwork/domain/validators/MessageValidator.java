package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

public class MessageValidator implements Validator<Message>{
    @Override
    public void validate(Message entity) throws ValidationException {
        StringBuilder s = new StringBuilder();
        if(entity.getText().length() == 0 || entity.getText().length() >= 256)
            s.append("Text incorect");
        if(s.toString().length() > 0)
            throw new ValidationException(s.toString());
    }
}
