package socialnetwork.domain;

import java.time.LocalDateTime;

public class SocialEvent extends Entity<Long> {
    private LocalDateTime date;
    private String description;
    private String title;
    private Utilizator userAttached;

    public SocialEvent(String title, String description, LocalDateTime date){

        this.title = title;
        this.description = description;
        this.date = date;
    }

    public SocialEvent(String title, String description, LocalDateTime date, Utilizator user){
        this(title, description, date);
        this.userAttached = user;
    }

    public SocialEvent(Long id, String title, String description, LocalDateTime date){
        this(title, description, date);
        this.setId(id);
    }

    public Utilizator getUserAttached() {
        return userAttached;
    }

    public void setUserAttached(Utilizator userAttached) {
        this.userAttached = userAttached;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
