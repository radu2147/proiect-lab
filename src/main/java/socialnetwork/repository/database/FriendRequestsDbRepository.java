package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashMap;

public class FriendRequestsDbRepository extends AbstractDbRepository<Tuple<Utilizator, Utilizator>, FriendRequest> {


    public FriendRequestsDbRepository(String url, String username, String password, Validator<FriendRequest> validator){

        super(url, username, password, validator);
    }

    @Override
    public FriendRequest findOne(Tuple<Utilizator, Utilizator> aLong) {
        String sqlQuerry = "SELECT * from friendrequests where sender=" + aLong.getLeft().getId() + " and recv=" + aLong.getRight().getId() + ";";
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<FriendRequest> findAll() {
        String querry = "SELECT * from friendrequests";
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public Iterable<FriendRequest> find(HashMap<String, String> where, String order, boolean dir) {
        String sqlQuerry = "SELECT * from friendrequests";
        setQuery(sqlQuerry);
        return super.find(where, order, dir);
    }

    @Override
    public Iterable<FriendRequest> find(HashMap<String, String> where) {
        String sqlQuerry = "SELECT * from friendrequests";
        setQuery(sqlQuerry);
        return super.find(where);
    }

    @Override
    public Iterable<FriendRequest> find(HashMap<String, String> where, int offset, int limit) {
        String sqlQuerry = "SELECT * from friendrequests";
        setQuery(sqlQuerry);
        return super.find(where, offset, limit);
    }

    @Override
    public Iterable<FriendRequest> find(HashMap<String, String> where, String order, boolean dir, int offset, int limit) {
        String sqlQuerry = "SELECT * from friendrequests";
        setQuery(sqlQuerry);
        return super.find(where, order, dir, offset, limit);
    }

    @Override
    public FriendRequest save(FriendRequest entity) {
        String sqlQuery = "insert into friendrequests(sender, recv, status, date) values(" + entity.getId().getLeft().getId() + "," + entity.getId().getRight().getId() + ", '" + entity.getStatus().toString() + "','" + entity.getDate().toString() + "');";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public FriendRequest delete(Tuple<Utilizator, Utilizator> aLong) {
        String sqlQuery = "delete from friendrequests where sender=" + aLong.getLeft().getId() + " and recv="+aLong.getRight().getId();
        setQuery(sqlQuery);
        return super.delete(aLong);
    }

    @Override
    public FriendRequest update(FriendRequest entity) {
        String sqlQuery = "update friendrequests set status='" + entity.getStatus() + "' where sender=" + entity.getId().getLeft().getId() + " and recv=" + entity.getId().getRight().getId() + ";";
        setQuery(sqlQuery);
        return super.update(entity);
    }

    @Override
    protected FriendRequest mapDbObject(ResultSet resultSet) throws SQLException {
        Long id1 = resultSet.getLong("sender");
        Long id2 = resultSet.getLong("recv");
        Status status = Status.valueOf(resultSet.getString("status"));
        LocalDateTime date = LocalDateTime.parse(resultSet.getString("date"));

        FriendRequest pr = new FriendRequest(new Utilizator(id1, "", ""), new Utilizator(id2, "", ""), status, date);
        return pr;

    }
}
