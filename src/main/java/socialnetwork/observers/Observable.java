package socialnetwork.observers;

import java.util.ArrayList;
import java.util.List;

public class Observable {

    public Observable(){
        lista = new ArrayList<>();
    }

    List<Observer> lista;

    public void remove_observer(Observer obs){
        lista.remove(obs);
    }

    public void add_observer(Observer obs){
        lista.add(obs);
    }

    public void update(){
        for(Observer obs: lista){
            obs.observe();
        }
    }
}
