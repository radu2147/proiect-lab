package socialnetwork.ui;

public interface UI {

    /**
     * Function that sets the ui object to process user request
     */
    void run();
}
