package socialnetwork.repository.database;
import socialnetwork.domain.Message;
import socialnetwork.domain.ReplyMessage;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;

public class MessageDbRepository extends AbstractDbRepository<Long, Message> {


    public MessageDbRepository(String url, String username, String password, Validator<Message> validator) {
        super(url, username, password, validator);
    }

    @Override
    public Message findOne(Long aLong) {
        String sqlQuerry = "SELECT * from messages where id=" + aLong;
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }


    @Override
    public Iterable<Message> findAll() {
        String querry = "SELECT * from messages";
        setQuery(querry);
        return super.findAll();
    }

    public MessageDbRepository() {
        super();
    }

    @Override
    public Iterable<Message> find(HashMap<String, String> where, String order, boolean dir) {
        String sqlQuerry = "SELECT * from messages";
        setQuery(sqlQuerry);
        return super.find(where, order, dir);
    }

    @Override
    public Iterable<Message> find(HashMap<String, String> where) {
        String sqlQuerry = "SELECT * from messages";
        setQuery(sqlQuerry);
        return super.find(where);
    }

    @Override
    public Iterable<Message> find(HashMap<String, String> where,int offset, int limit) {
        String sqlQuerry = "SELECT * from messages";
        setQuery(sqlQuerry);
        return super.find(where, offset, limit);
    }

    @Override
    public Iterable<Message> find(HashMap<String, String> where, String order, boolean dir, int offset, int limit) {
        String sqlQuerry = "SELECT * from messages";
        setQuery(sqlQuerry);
        return super.find(where, order, dir, offset,limit);
    }

    @Override
    public Message save(Message entity) {
        String sqlQuery;
        if(entity instanceof ReplyMessage)
            sqlQuery = "insert into messages(id1, id2, date, text, replyto) values(" + entity.getFrom().getId() + ", " + entity.getTo().get(0).getId() + ", '" + entity.getDate().toString() + "', '" + entity.getText() + "', " + ((ReplyMessage) entity).getReplyTo().getId() + ");";
        else
            sqlQuery = "insert into messages(id1, id2, date, text) values(" + entity.getFrom().getId() + ", " + entity.getTo().get(0).getId() + ", '" + entity.getDate().toString() + "', '" + entity.getText() + "');";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public Message delete(Long aLong) {
        String sqlQuery = "delete from messages where id=" + aLong;
        setQuery(sqlQuery);
        return super.delete(aLong);

    }

    public Iterable<Message> getConversation(Long sender, Long recv, int offset, int limit){
        String querry = "Select * from messages where id1="+sender+" and id2=" + recv + " or id1="+recv + " and id2=" + sender + " order by date desc offset " + offset + " limit " + limit;
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

    @Override
    protected Message mapDbObject(ResultSet resultSet) throws SQLException {
        Long id1 = resultSet.getLong("id1");
        Long id2 = resultSet.getLong("id2");
        Long idm = resultSet.getLong("replyto");
        LocalDateTime date = LocalDateTime.parse(resultSet.getString("date"));
        String text = resultSet.getString("text");
        Message mes;
        if(idm == 0) {
            mes = new Message(text, date);
            mes.setFrom(new Utilizator(id1, null, null));
            mes.addTo(new Utilizator(id2, null, null));
            mes.setId(resultSet.getLong("id"));
        }
        else{
            Message m = new Message("Test", null);
            mes = new ReplyMessage(text, date, m);
            mes.setId(resultSet.getLong("id"));
            mes.setFrom(new Utilizator(id1, null, null));
            mes.addTo(new Utilizator(id2, null, null));
            m.setId(idm);
        }
        return mes;
    }
}
