package socialnetwork.domain.validators;

public interface Validator<T> {
    /**
     * Function that validates the object of type T
     *
     * @param entity
     * @throws ValidationException
     */
    void validate(T entity) throws ValidationException;
}