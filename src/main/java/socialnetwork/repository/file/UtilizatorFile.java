package socialnetwork.repository.file;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.util.List;

public class UtilizatorFile extends AbstractFileRepository<Long, Utilizator> {

    public UtilizatorFile(String fileName, Validator<Utilizator> validator) {
        super(fileName, validator);
    }

    /**
     *
     * @param attributes
     * @return
     *          deserialzation of the user object from file to be loaded in memory
     */
    @Override
    public Utilizator extractEntity(List<String> attributes) {
        Utilizator user = new Utilizator(Long.parseLong(attributes.get(0)), attributes.get(1),attributes.get(2));

        return user;
    }

    /**
     *
     * @param entity
     * @return
     *          serialization of Utilizator object to be written in file
     */
    @Override
    protected String createEntityAsString(Utilizator entity) {
        return entity.getId()+";"+entity.getFirstName()+";"+entity.getLastName();
    }


}
