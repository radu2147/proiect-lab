package socialnetwork.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import socialnetwork.domain.UserDTO;
import socialnetwork.export.Export;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ExportView extends Controller{

    private UserDTO user;

    @FXML
    Button button;

    @FXML
    DatePicker start;

    @FXML
    DatePicker end;

    @FXML
    LineChart<String, Number> linechart;

    @FXML
    CategoryAxis xAxis;

    @FXML
    NumberAxis yAxis;

    @Override
    public void initialize() {
        if(user != null) {
            XYChart.Series<String, Number> series = new XYChart.Series<>();
            HashMap<String, Integer> rez = page.countMessagesPerDate(user.getId());
            List<String> keys = rez.keySet().stream().sorted().collect(Collectors.toList());
            for (String el : keys) {
                series.getData().add(new XYChart.Data<>(el, rez.get(el)));
            }
            linechart.getData().add(series);
        }
        button.setOnMouseClicked(e -> {
            try{
                LocalDateTime startDate, endDate;
                if(start.getEditor().getText().equals(""))
                    startDate = LocalDateTime.of(1970,1,1,0,0);
                else {
                    String[] args = start.getEditor().getText().split("/");
                    startDate = LocalDateTime.of(Integer.parseInt(args[2]), Integer.parseInt(args[0]), Integer.parseInt(args[1]), 0, 0);
                }
                if(end.getEditor().getText().equals(""))
                    endDate = LocalDateTime.now();
                else {
                    String[] args = start.getEditor().getText().split("/");
                    endDate = LocalDateTime.of(Integer.parseInt(args[2]), Integer.parseInt(args[0]), Integer.parseInt(args[1]), 0, 0);
                }
                Export.exportConversation(page.filterMessage(user.getId(), startDate, endDate), page.getCurrentUser(), user);
            }
            catch(Exception es){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText(es.toString());
                a.show();
                es.printStackTrace();
            }
            ((Stage) button.getScene().getWindow()).close();
        });
    }

    public void setUser(UserDTO userDTO){
        user = userDTO;
    }

    @Override
    public void observe() {

    }
}

