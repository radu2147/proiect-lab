package socialnetwork.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilizator extends Entity<Long> {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private List<Utilizator> friends;
    private boolean notifications;

    public Utilizator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        friends = new ArrayList<>();
        setId(Long.valueOf(hashCode()));
    }

    public Utilizator(Long id, String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        setId(id);
    }

    public Utilizator(Long id, String firstName, String lastName, String username, String password, boolean notifications){
        this(firstName, lastName, username, password, notifications);
        setId(id);
    }

    public Utilizator(String firstName, String lastName, String username, String password, boolean notifications){
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.notifications = notifications;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Utilizator> getFriends() {
        return friends;
    }

    public void addFriend(Utilizator u){
        friends.add(u);
    }

    private String friendsList(){
        StringBuilder mes = new StringBuilder("");
        for(Utilizator el: friends){
            mes.append(el.firstName +  " " + el.lastName + ", ");
        }
        return (mes.toString().equals("")) ? "None" : mes.toString();
    }

    @Override
    public String toString() {
        return display_name();
    }

    public String display_name(){
        return firstName + "_" + lastName + "_" + getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isNotifications() {
        return notifications;
    }

    public void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }
}