package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.util.HashMap;

public class UtilizatorDbRepository extends AbstractDbRepository<Long, Utilizator> {

    public UtilizatorDbRepository(String url, String username, String password, Validator<Utilizator> validator) {
        super(url, username, password, validator);
    }
    @Override
    public Utilizator findOne(Long aLong) {
        String sqlQuerry = "SELECT * from users where id=" + aLong;
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<Utilizator> findAll() {
        String querry = "SELECT * from users";
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public Utilizator save(Utilizator entity) {
        String sqlQuery = "insert into users(first_name, last_name, username, password, notifications) values('" + entity.getFirstName() + "','" + entity.getLastName() +"','" + entity.getUsername() + "','" + entity.getPassword() +"'," + entity.isNotifications() + ");";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public Utilizator delete(Long aLong) {
        String sqlQuery = "delete from users where id=" + aLong;
        setQuery(sqlQuery);
        return super.delete(aLong);

    }

    @Override
    public Utilizator update(Utilizator entity) {
        String sqlQuery = "update users set first_name='" + entity.getFirstName() + "', last_name='" + entity.getLastName() + "', username='" + entity.getUsername() + "', password='" + entity.getPassword() + "', notifications=" + entity.isNotifications() +  " where id=" + entity.getId() + ";";
        System.out.println(sqlQuery);
        setQuery(sqlQuery);
        return super.update(entity);
    }

    @Override
    protected Utilizator mapDbObject(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String username = resultSet.getString("username");
        String password = resultSet.getString("password");
        boolean notifications = resultSet.getBoolean("notifications");

        return new Utilizator(id, firstName, lastName, username, password, notifications);

    }

    @Override
    public Iterable<Utilizator> find(HashMap<String, String> where, String order, boolean dir) {
        String sqlQuerry = "SELECT * from users";
        setQuery(sqlQuerry);
        return super.find(where, order, dir);
    }

    @Override
    public Iterable<Utilizator> find(HashMap<String, String> where) {
        String sqlQuerry = "SELECT * from users";
        setQuery(sqlQuerry);
        return super.find(where);
    }

    @Override
    public Iterable<Utilizator> find(HashMap<String, String> where, int offset, int limit) {
        String sqlQuerry = "SELECT * from users";
        setQuery(sqlQuerry);
        return super.find(where, offset, limit);
    }

    @Override
    public Iterable<Utilizator> find(HashMap<String, String> where, String order, boolean dir, int offset,  int limit) {
        String sqlQuerry = "SELECT * from users";
        setQuery(sqlQuerry);
        return super.find(where, order, dir, offset, limit);
    }

    public Utilizator findOne(String user_name) {
        String query = "select * from users where username='" + user_name + "'";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery()){
            if(resultSet.next()) {
                return mapDbObject(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
