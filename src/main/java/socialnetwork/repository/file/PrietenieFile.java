package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

public class PrietenieFile extends AbstractFileRepository<Tuple<Long, Long>, Prietenie> {
    public PrietenieFile(String fileName, Validator<Prietenie> validator) {
        super(fileName, validator);
    }

    /**
     *
     * @param attributes
     * @return
     *          deserialzation of the Prietenie object from file to be loaded in memory
     */
    @Override
    public Prietenie extractEntity(List<String> attributes) {
        String[] attr = attributes.get(0).split(",");
        Tuple<Long, Long> tuple = new Tuple<>(Long.valueOf(attr[0]), Long.valueOf(attr[1]));
        Prietenie a = new Prietenie(tuple);
        a.setDate(LocalDateTime.parse(attributes.get(1)));
        return a;
    }

    /**
     *
     * @param entity
     * @return
     *          serialization of Prietenie object to be written in file
     */
    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId() + ";" + entity.getDate();
    }
}
