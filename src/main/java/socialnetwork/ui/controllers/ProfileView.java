package socialnetwork.ui.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.Utils;
import socialnetwork.domain.SocialEvent;

import java.time.LocalDateTime;
import java.util.List;

public class ProfileView extends Controller{

    ObservableList<SocialEvent> eventsListToday = FXCollections.observableArrayList();
    ObservableList<SocialEvent> eventsList = FXCollections.observableArrayList();

    @FXML
    TableView<SocialEvent> events;

    @FXML
    TableColumn<SocialEvent, String> user;

    @FXML
    TableColumn<SocialEvent, String> title;

    @FXML
    TableColumn<SocialEvent, String> desc;

    @FXML
    TableColumn<SocialEvent, LocalDateTime> date;

    @FXML
    TableView<SocialEvent> events0;

    @FXML
    TableColumn<SocialEvent, String> user0;

    @FXML
    TableColumn<SocialEvent, String> title0;

    @FXML
    TableColumn<SocialEvent, String> desc0;

    @FXML
    TableColumn<SocialEvent, LocalDateTime> date0;

    @FXML
    TableColumn<SocialEvent, String> subscribed;

    @FXML
    MenuItem home;

    @FXML
    MenuItem friends;

    @FXML
    Label name;

    @FXML
    MenuItem logout;

    @FXML
    MenuItem requests;

    @FXML
    Button add_event;

    @FXML
    Hyperlink loader;

    @FXML
    Button subscribe;

    @FXML
    CheckBox notifications;



    public void initialize(){
        if(page != null){
            logout.setOnAction(e -> {
                page.logout();
            });
            notifications.setSelected(page.getUser().isNotifications());
            notifications.selectedProperty().addListener((observable, oldValue, newValue) -> {
                page.getUser().setNotifications(newValue);
            });
            add_event.setOnMouseClicked(e -> {
                try{
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/views/create_event_view.fxml"));
                    AnchorPane pane = loader.load();
                    CreateEventView controller = loader.getController();
                    controller.setPage(page);
                    controller.initialize();
                    change_controller(controller);
                    Stage stage = new Stage();
                    stage.setScene(new Scene(pane, 552, 620));
                    stage.setTitle("Pied Piper");
                    stage.show();
                }
                catch (Exception ec){
                    ec.printStackTrace();
                }
            });

            eventsList.setAll(Utils.iterablToList(page.getEvents()));
            events.setItems(eventsList);

            title.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getTitle()));
            desc.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getDescription()));
            date.setCellValueFactory(new PropertyValueFactory<>("date"));
            user.setCellValueFactory(new PropertyValueFactory<>("userAttached"));

            events.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                subscribe.setVisible(newValue != null);
                if(newValue != null) {
                    if (page.isSubscribed(newValue.getId())) {
                        subscribe.setText("- Unsubscribe");
                        subscribe.setOnMouseClicked(e -> {
                            page.unsubscribe(newValue.getId());
                            subscribe.setText("+ Subscribe");
                        });

                    } else {
                        subscribe.setText("+ Subscribe");
                        subscribe.setOnMouseClicked(e -> {
                            page.subscribe(newValue.getId());
                            subscribe.setText("- Unsubscribe");
                        });

                    }
                }
            });

            loader.setOnAction(e ->{

                List<SocialEvent> as = Utils.iterablToList(page.getEventsPage(eventsList.size(), 5));
                as.addAll(eventsList);
                eventsList.setAll(as);
                page.setEvents(as);
                events.setItems(eventsList);
            });

            eventsListToday.setAll(Utils.iterablToList(page.getTodayEvents()));
            events0.setItems(eventsListToday);

            title0.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getTitle()));
            desc0.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getDescription()));
            date0.setCellValueFactory(new PropertyValueFactory<>("date"));
            user0.setCellValueFactory(new PropertyValueFactory<>("userAttached"));

            subscribed.setCellValueFactory(e -> new SimpleStringProperty(page.isSubscribed(e.getValue().getId()) ? "Subscribed" : ""));

            name.setText(page.getCurrentUser().getFirstName() + " " +  page.getCurrentUser().getLastName());
        }
        home.setOnAction(e -> switch_view("/views/user_view.fxml", name));
        friends.setOnAction(e -> switch_view("/views/friends_view.fxml", name));
        requests.setOnAction(e -> switch_view("/views/requests_view.fxml", name));
    }

    @Override
    public void observe() {

    }
}
