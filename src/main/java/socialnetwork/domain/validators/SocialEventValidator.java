package socialnetwork.domain.validators;

import socialnetwork.domain.SocialEvent;

import java.time.LocalDateTime;

public class SocialEventValidator implements Validator<SocialEvent> {
    @Override
    public void validate(SocialEvent entity) throws ValidationException {
        if(entity.getDate().isBefore(LocalDateTime.now())){
            throw new ValidationException("Date incorrect");
        }
    }
}
