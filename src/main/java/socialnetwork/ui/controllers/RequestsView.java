package socialnetwork.ui.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Status;

import java.time.LocalDateTime;


public class RequestsView extends Controller{

    public MenuItem messenger;
    ObservableList<FriendRequest> list = FXCollections.observableArrayList();

    @FXML
    MenuItem profile;

    @FXML
    MenuItem friends;

    @FXML
    MenuItem  home;

    @FXML
    Label name;

    @FXML
    MenuItem logout;

    @FXML
    TextField nameSearch;

    @FXML
    TableView<FriendRequest> people;

    @FXML
    TableColumn<FriendRequest, String> columnPrenume;

    @FXML
    TableColumn<FriendRequest, String> columnNume;

    @FXML
    TableColumn<FriendRequest, LocalDateTime> columnDate;

    @FXML
    TableColumn<FriendRequest, Status> columnStatus;

    @FXML
    Button accept;

    @FXML
    Button reject;

    @FXML
    HBox response;

    @FXML
    Button status;

    @FXML
    VBox buttons;

    public void initialize(){

        if(page != null){
            list.setAll(page.getRequestOfUser());
            people.setItems(list);
            name.setText(page.getCurrentUser().getFirstName() + " " + page.getCurrentUser().getLastName());
            logout.setOnAction(event -> {
                page.logout();
            });
            nameSearch.textProperty().addListener((observable, oldValue, newValue) -> {
                list.setAll(page.getRequestOfUser());
                people.setItems(list);
            });
            accept.setOnMouseClicked(e -> {
                page.respond_req(page.getFriendRequestEndPoint(people.getSelectionModel().getSelectedItem()).getId(), Status.ACCEPTED);
                response.setVisible(false);
                status.setText("You are friends");
            });
            reject.setOnMouseClicked(e -> {
                page.respond_req(page.getFriendRequestEndPoint(people.getSelectionModel().getSelectedItem()).getId(), Status.REJECTED);
                response.setVisible(false);
                list.setAll(page.getRequestOfUser());
                people.setItems(list);
            });
            try {
                people.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    buttons.setVisible(people.getSelectionModel().getSelectedItem() != null);
                    if(people.getSelectionModel().getSelectedItem() != null) {
                        if(page.areFriends(page.getFriendRequestEndPoint(people.getSelectionModel().getSelectedItem()).getId())){
                            status.setText("You are friends");
                            response.setVisible(false);
                        }
                        else if (page.hasSentRequest(page.getFriendRequestEndPoint(people.getSelectionModel().getSelectedItem()).getId())) {
                            status.setText(":) Request sent");
                            response.setVisible(false);
                        }
                        else if (page.hasReceivedRequest(page.getFriendRequestEndPoint(people.getSelectionModel().getSelectedItem()).getId())) {
                            status.setText("? Pending");
                            response.setVisible(true);
                        }
                    }
                });

            }
            catch (Exception ex){
                ex.printStackTrace();
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText(ex.toString());
                a.show();

            }
            status.setOnMouseClicked(e -> {
                if(status.getText().equals(":) Request sent")){
                    page.delFrReq(page.getFriendRequestEndPoint(people.getSelectionModel().getSelectedItem()).getId());
                    list.setAll(page.getRequestOfUser());
                    people.setItems(list);
                }
            });
            columnPrenume.setCellValueFactory(param -> new SimpleStringProperty(page.getFriendRequestEndPoint(param.getValue()).getFirstName()));
            columnNume.setCellValueFactory(param -> new SimpleStringProperty(page.getFriendRequestEndPoint(param.getValue()).getLastName()));
            columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            columnStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        }
        profile.setOnAction(e -> switch_view("/views/profile_view.fxml", name));
        friends.setOnAction(e -> switch_view("/views/friends_view.fxml", name));
        home.setOnAction(e -> switch_view("/views/user_view.fxml", name));
        messenger.setOnAction(e -> switch_view("/views/messenger_view.fxml", name));

    }

    @Override
    public void observe() {

    }
}
