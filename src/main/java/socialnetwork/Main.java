package socialnetwork;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import socialnetwork.domain.Utilizator;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.FXUi;

import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {
//        String fileName= ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
//        Repository<Long,Utilizator> userFileRepository = new UtilizatorFile(fileName
//                , new UtilizatorValidator());


        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        Repository<Long, Utilizator> userFileRepository3 =
                new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());
        Repository<Tuple<Long, Long>, Prietenie> friendshipFileRepository3 = new FriendshipDbRepository(url, username, pasword, new PrietenieValidator());
        Repository<Tuple<Utilizator, Utilizator>, FriendRequest> friendreqsDbRepository3 = new FriendRequestsDbRepository(url, username, pasword, new RequestValidator());
        AbstractDbRepository<Long, Message> messageRepository = new MessageDbRepository(url, username, pasword, new MessageValidator());


        FXUi.main(args);

    }
}


