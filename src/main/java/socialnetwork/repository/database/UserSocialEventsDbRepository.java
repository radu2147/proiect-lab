package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.util.HashMap;

public class UserSocialEventsDbRepository extends AbstractDbRepository<Tuple<Long, Long>, Entity<Tuple<Long, Long>>> {

    public UserSocialEventsDbRepository(String url, String username, String password, Validator<Entity<Tuple<Long, Long>>> valid){
        super(url, username, password, valid);
    }

    @Override
    public Entity<Tuple<Long, Long>> save(Entity<Tuple<Long, Long>> entity) {
        String query = "insert into usersocialevents(userid, socialeventid) values(" + entity.getId().getLeft() + ", " + entity.getId().getRight() + ");";
        setQuery(query);
        return super.save(entity);
    }

    @Override
    public Iterable<Entity<Tuple<Long, Long>>> findAll() {
        setQuery("select * from usersocialevents");
        return super.findAll();
    }

    @Override
    public Entity<Tuple<Long, Long>> findOne(Tuple<Long, Long> longLongTuple) {
        setQuery("select * from usersocialevents where userid=" + longLongTuple.getLeft() + " and socialeventid=" + longLongTuple.getRight() + ";");
        return super.findOne(longLongTuple);
    }

    @Override
    public Entity<Tuple<Long, Long>> delete(Tuple<Long, Long> longLongTuple) {
        setQuery("delete from usersocialevents where userid=" + longLongTuple.getLeft() + " and socialeventid=" + longLongTuple.getRight() + ";");
        return super.delete(longLongTuple);
    }

    @Override
    public Iterable<Entity<Tuple<Long, Long>>> find(HashMap<String, String> where, int offset, int limit) {
        setQuery("select * from usersocialevents");
        return super.find(where, offset, limit);
    }

    @Override
    protected Entity<Tuple<Long, Long>> mapDbObject(ResultSet resultSet) throws SQLException {
        Long eventid = resultSet.getLong("socialeventid");
        Long userid = resultSet.getLong("userid");
        Entity<Tuple<Long, Long>> rez = new Entity<>();
        rez.setId(new Tuple<>(userid, eventid));
        return rez;
    }

    public void deleteByEvent(Long id){
        String querry = "delete from usersocialevents where socialeventid=" + id;
        setQuery(querry);
        super.ddl();
    }
}
