package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendRequest extends Entity<Tuple<Utilizator, Utilizator>> {

    private Status status;
    private LocalDateTime date;
    public FriendRequest(Utilizator id1, Utilizator id2, Status status, LocalDateTime date){
        setId(new Tuple<>(id1, id2));
        this.status = status;
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return getDate() + " From: " + getId().getLeft() + " To: " + getId().getRight();
    }
}
