package socialnetwork;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Utils {
    public static <Type> List<Type> iterablToList(Iterable<Type> iterable){
        return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
    }

    public static <T> HashMap<T, Integer> mergeMaps(HashMap<T, Integer> a, HashMap<T, Integer> b){
        for(T key: b.keySet()){
            if(a.containsKey(key)){
                a.put(key, b.get(key) + a.get(key));
            }
            else{
                a.put(key, b.get(key));
            }
        }
        return a;
    }

    public static boolean isToday(LocalDateTime date1, LocalDateTime date2){
        return date1.getYear() == date2.getYear() && date1.getMonth() == date2.getMonth() && date1.getDayOfMonth() == date2.getDayOfMonth();
    }
}
