package socialnetwork.ui.controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import socialnetwork.Utils;
import socialnetwork.domain.Utilizator;
import socialnetwork.observers.Observer;
import socialnetwork.service.Page;

public class LoginView implements Observer {

    private Page page;

    private Stage stage;

    @FXML
    TextField login_user;

    @FXML
    Label error;

    @FXML
    VBox layout;

    @FXML
    Button button;

    @FXML
    CheckBox remember;

    @FXML
    PasswordField login_pass;

    @FXML
    Button sign_up;

    public void notifyUser(Page page){
        if(page.getUser().isNotifications()) {
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    Thread.sleep(2000);
                    return null;
                }
            };
            task.setOnSucceeded(event -> {
                int nr = Utils.iterablToList(page.getTodayEvents()).size();
                if (nr > 0) {
                    Alert a = new Alert(Alert.AlertType.INFORMATION);

                    a.setContentText("Aveti " + nr + " evenimente astazi");
                    a.show();
                }
            });
            task.setOnFailed(e -> System.out.println("BOU"));

            new Thread(task).start();
        }
    }

    public void initialize(){
        if(page != null){
            Utilizator user = page.isUserRemembered();
            if(user != null){
                try {
                    page.fastLogin(user);
                }
                catch(Exception ex){
                    error.setVisible(true);
                    ex.printStackTrace();
                }
            }
        }
        layout.setAlignment(Pos.CENTER);
        button.setOnMouseClicked(e -> {
            try {
                page.login_user(login_user.getText(), login_pass.getText());
                if(remember.isSelected()){
                    page.rememberUser(page.getUser());
                }
            }
            catch(Exception ex){
                error.setVisible(true);
            }
        });
        sign_up.setOnMouseClicked(e -> {
            try {
                Stage stage = (Stage) button.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/register_view.fxml"));
                AnchorPane pane = loader.load();
                RegisterView reg = loader.getController();
                reg.setPage(page);
                reg.setStage(stage);

                stage.setScene(new Scene(pane, 450, 420));
            }
            catch(Exception es){
                es.printStackTrace();
            }
        });

    }

    @Override
    public void observe() {
        if(page.anonymousSession()) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/login_view.fxml"));
                AnchorPane pane = loader.load();
                LoginView controller = loader.getController();
                controller.setPage(page);
                stage.setScene(new Scene(pane, 450, 300));
                controller.setStage(stage);
            }
            catch (Exception es) {
                es.printStackTrace();
            }
        }
        else{
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/user_view.fxml"));
                AnchorPane pane = loader.load();
                UserView controller = loader.getController();
                page.load();
                controller.setPage(page);
                notifyUser(page);
                controller.initialize();
                stage.setScene(new Scene(pane, 600, 650));
            } catch (Exception es) {
                es.printStackTrace();
            }
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
