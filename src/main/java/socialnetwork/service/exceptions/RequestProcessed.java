package socialnetwork.service.exceptions;

public class RequestProcessed extends RuntimeException{
    public RequestProcessed(String message){
        super(message);
    }
}
