package socialnetwork.service.exceptions;

public class ExistingUserAlreadyLoggedIn extends RuntimeException{
    public ExistingUserAlreadyLoggedIn(String a){
        super(a);
    }
}
