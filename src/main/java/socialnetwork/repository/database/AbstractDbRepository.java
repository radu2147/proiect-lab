package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.observers.Observer;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.*;

public abstract class AbstractDbRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {

    protected String query;
    protected String url;
    protected String username;
    protected String password;
    protected Validator<E> validator;
    private List<Observer> lista = new ArrayList<>();

    public AbstractDbRepository(String url, String username, String password, Validator<E> validator){

        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public AbstractDbRepository() {
        super();
    }

    public final void remove_observer(Observer obs){
        lista.remove(obs);
    }

    public final void add_observer(Observer obs){
        lista.add(obs);
    }

    public final void update(){
        for(Observer obs: lista){
            obs.observe();
        }
    }

    private String addWhere(String querry, HashMap<String, String> where){
        if(!where.isEmpty()) {
            querry += " where ";
            int i = 0;
            for (String key : where.keySet()) {
                querry += key + "=" + where.get(key) + " ";
                i += 1;
                if (i != where.size()) {
                    querry += "or ";
                }
            }
        }
        return querry;
    }

    private String addOrder(String querry, String order, boolean dir){
        querry += "order by " + order + (dir ? " asc" : " desc");
        return querry;
    }

    private String finish(String querry){
        return (querry + ";");
    }

    private String limit(String querry, int offset, int limit){
        querry += " offset " + offset + " limit " + limit;
        return querry;
    }


    @Override
    public E findOne(ID id) {
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery()){
            if(resultSet.next()) {

                return mapDbObject(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public Iterable<E> findAll() {
        Set<E> prSet = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                prSet.add(mapDbObject(resultSet));
            }
            return prSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prSet;
    }

    @Override
    public E save(E entity) {
        validator.validate(entity);
        String save = query;
        E pr = findOne(entity.getId());
        query = save;
        if(pr == null){
            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement(query)
            ){
                statement.executeUpdate();
                update();
                return null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return entity;
    }

    @Override
    public E delete(ID id) {
        String save = query;
        E u = findOne(id);
        query = save;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            ){
            statement.executeUpdate();
            update();
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<E> find(HashMap<String, String> where, String order, boolean dir) {
        query = finish(addOrder(addWhere(query, where), order, dir));
        Set<E> prSet = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                prSet.add(mapDbObject(resultSet));
            }
            return prSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prSet;
    }

    @Override
    public Iterable<E> find(HashMap<String, String> where) {
        setQuery(finish(addWhere(query, where)));
        Set<E> prSet = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                prSet.add(mapDbObject(resultSet));
            }
            return prSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prSet;
    }

    @Override
    public E update(E entity) {
        validator.validate(entity);
        String save = query;
        E u = findOne(entity.getId());
        query = save;
        if(u != null){
            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement(query);
            ){
                statement.executeUpdate();
                update();
                return entity;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    final public int size() {
        return 0;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public Iterable<E> find(HashMap<String, String> where, int offset, int limit) {
        setQuery(limit(addWhere(query, where), offset, limit)) ;
        Set<E> prSet = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                prSet.add(mapDbObject(resultSet));
            }
            return prSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prSet;
    }

    @Override
    public Iterable<E> find(HashMap<String, String> where, String order, boolean dir, int offset, int limit) {
        query = limit(addOrder(addWhere(query, where), order, dir), offset, limit);
        SortedSet<E> prSet = new TreeSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                prSet.add(mapDbObject(resultSet));
            }
            return prSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prSet;
    }

    public final void setQuery(String querry){
        this.query = querry;
    }

    protected abstract E mapDbObject(ResultSet resultSet) throws SQLException;


    public void ddl(){
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
        ){
            statement.executeUpdate();
            update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}