package socialnetwork.export;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.HorizontalAlignment;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Message;
import socialnetwork.domain.UserDTO;
import socialnetwork.domain.Utilizator;

public class Export {
    public static void exportConversation(Iterable<Message> msg, Utilizator from, UserDTO to){
        try {
            PdfWriter writer = new PdfWriter("mesaje.pdf");

            PdfDocument pdfDoc = new PdfDocument(writer);
            // Adding a new page
            pdfDoc.addNewPage();

            // Creating a Document
            Document document = new Document(pdfDoc);
            document.add(new Paragraph("Mesajele utilizatorului " + from.getFirstName() + " " + from.getLastName() + " cu " + to.getFirst_name() + " " + to.getLast_name()).setBold().setFontSize(22f).setHorizontalAlignment(HorizontalAlignment.CENTER).setMarginLeft(80f));

            document.add(new Paragraph("Mesaje").setUnderline());
            msg.forEach(e ->{
                document.add(new Paragraph(e.toString() + "\n"));
            });

            // Closing the document
            document.close();

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    public static void exportActivities(Iterable<? extends Entity<?>> msg, Iterable<? extends Entity<?>> reqs, Utilizator user){
        try{
            PdfWriter writer = new PdfWriter("activitati.pdf");

            PdfDocument pdfDoc = new PdfDocument(writer);
            pdfDoc.addNewPage();
            Document document = new Document(pdfDoc);
            document.add(new Paragraph("Activitatile utilizatorului " + user.getUsername()).setBold().setFontSize(22f).setMarginLeft(85f));

            document.add(new Paragraph("Mesaje").setUnderline().setFontSize(18f));

            msg.forEach(e ->{
                document.add(new Paragraph(e.toString() + "\n"));
            });
            document.add(new Paragraph("Cereri de prietenie acceptate").setUnderline().setFontSize(15f));
            reqs.forEach(e -> {
                document.add(new Paragraph(e.toString() + "\n"));
            });
            document.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
