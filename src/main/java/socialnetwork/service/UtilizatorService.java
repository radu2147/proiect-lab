package socialnetwork.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import socialnetwork.Utils;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.encryption.Crypter;
import socialnetwork.observers.Observable;
import socialnetwork.observers.Observer;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.exceptions.ExistingUserAlreadyLoggedIn;
import socialnetwork.service.exceptions.NoUserLoggedIn;
import socialnetwork.service.exceptions.UserNotExists;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class UtilizatorService extends Observable {
    private final Repository<Long, Utilizator> repo;
    private final Repository<Long, Utilizator> cachedUser;
    private final Repository<Tuple<Long, Long>, Prietenie> fr_repo;
    private final Repository<Long, Message> mess_repo;
    private Utilizator logged_user;
    private final Validator<String> validatorLogin;
    private Repository<Tuple<Utilizator, Utilizator>, FriendRequest> req_repo;
    private Repository<Long, SocialEvent> event_repo;
    private Repository<Tuple<Long, Long>, Entity<Tuple<Long, Long>>> usereventsrepo;

    public UtilizatorService(Repository<Long, Utilizator> repo, Repository<Tuple<Long, Long>, Prietenie> fr_repo, Repository<Long, Message> mess_repo, Repository<Tuple<Utilizator, Utilizator>, FriendRequest> req_repo, Repository<Long, SocialEvent> socialEventsDbRepository, Repository<Tuple<Long, Long>, Entity<Tuple<Long, Long>>> usereventsrepo, Repository<Long, Utilizator> cachedUser, Validator<String> validator) {
        this.repo = repo;
        this.fr_repo = fr_repo;
        this.mess_repo = mess_repo;
        this.event_repo = socialEventsDbRepository;
        this.usereventsrepo = usereventsrepo;
        this.req_repo = req_repo;
        this.cachedUser = cachedUser;
        this.logged_user = null;
        this.validatorLogin = validator;
    }

    /**
     * Saves the user in memory
     *
     * @param messageTask
     * @return
     *      Return result of repo.save function
     */
    public Utilizator addUtilizator(Utilizator messageTask) {
        messageTask.setPassword(Crypter.encryptPassword(messageTask.getPassword()));
        HashMap<String, String> a = new HashMap<>();
        a.put("username", "'" + messageTask.getUsername() + "'");
        List<Utilizator> x = Utils.iterablToList(repo.find(a));
        if(x.size() != 0)
            return x.get(0);
        return repo.save(messageTask);
    }

    /**
     * Removes the user and all its friendship object.
     *
     * @param id
     * @return
     *      returns the eliminated utilizator
     */
    public Utilizator removeUtilizator(Long id){

        Utilizator u = repo.delete(id);
        ArrayList<Prietenie> arr = new ArrayList<>();
        for(Prietenie el: fr_repo.findAll()){
            if(el.getId().getLeft().equals(id) || el.getId().getRight().equals(id)){
                arr.add(el);
            }
        }
        ArrayList<Message> arr2 = new ArrayList<>();
        for(Message m: mess_repo.findAll()){
            if(m.getFrom().getId().equals(id) || m.getTo().get(0).getId().equals(id))
                arr2.add(m);
        }

        ArrayList<FriendRequest> arr3 = new ArrayList<>();
        for(FriendRequest fr: req_repo.findAll()){
            if(fr.getId().getLeft().getId().equals(id) || fr.getId().getRight().getId().equals(id))
                arr3.add(fr);
        }
        arr3.forEach(e -> req_repo.delete(e.getId()));
        for(Message m: mess_repo.findAll()){
            if(m.getFrom().getId().equals(id) || m.getTo().get(0).getId().equals(id))
                arr2.add(m);
        }
        arr2.forEach(e -> mess_repo.delete(e.getId()));
        arr.forEach(pr->{
            fr_repo.delete(pr.getId());
            Long idToDel = (pr.getId().getLeft().equals(id)) ? pr.getId().getRight() : pr.getId().getLeft();
            repo.findOne(idToDel).getFriends().remove(u);
        });

        return u;
    }

    /**
     * Function
     * @return
     */
    private Iterable<Utilizator> getUsersWithFriends(){
        return repo.findAll();
    }

    /**
     * Returns all the Utilizator objects
     * @return
     *      Iterable object from repository
     */
    public Iterable<Utilizator> getAll(){
        return getUsersWithFriends();
    }

    public void login_user(String username, String pass){
        validatorLogin.validate(username);
        if(anonymousSession()){
            logged_user = ((UtilizatorDbRepository) repo).findOne(username);
            if((logged_user == null) ? true : !(BCrypt.verifyer().verify(pass.toCharArray(), logged_user.getPassword().toCharArray()).verified)) {
                logged_user = null;
                throw new UserNotExists("User does not exists");
            }

            update();
        }
        else{
            throw new ExistingUserAlreadyLoggedIn("Existing user already logged in");
        }

    }

    public Iterable<UserDTO> getNonMatchingFriends(String username){
        return filter(e -> e.display_name().contains(username) && !e.getId().equals(logged_user.getId()));
    }

    private Iterable<UserDTO> filter(Predicate<Utilizator> function){
        if(logged_user == null) throw new NoUserLoggedIn("No user is logged in");
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(function::test)
                .map(
                        e -> new UserDTO(e.getId(), e.getFirstName(), e.getLastName(), e.getUsername())
                )
                .collect(Collectors.toList());
    }
    public void deleteCachedUser(Utilizator user){
        cachedUser.delete(user.getId());
    }

    public void cleanEvents(){
        List<SocialEvent> a = StreamSupport.stream(event_repo.findAll().spliterator(), false)
                .filter(e -> e.getDate().isBefore(LocalDateTime.now()))
                .collect(Collectors.toList());
        for(SocialEvent el: a){
            event_repo.delete(el.getId());
            ((UserSocialEventsDbRepository) usereventsrepo).deleteByEvent(el.getId());
        }
    }

    public void logout(){
        if(logged_user != null){
            deleteCachedUser(logged_user);
            cleanEvents();
            logged_user = null;
            update();
            return;
        }
        throw new NoUserLoggedIn("No user is logged in");
    }

    public boolean anonymousSession(){
        return logged_user == null;
    }

    public Utilizator getCurrentUser(){
        return logged_user;
    }

    public void rememberUser(Utilizator user){
        cachedUser.save(user);
    }

    public Iterable<Message> filterMessage(Long id, LocalDateTime start, LocalDateTime end){
        return StreamSupport.stream(getConversation(id).spliterator(), false)
                .filter(e -> e.getDate().compareTo(start) > 0 && e.getDate().compareTo(end) < 0)
                .collect(Collectors.toList());

    }

    public Iterable<Message> filterMessagesAll(LocalDateTime start, LocalDateTime end){
        return StreamSupport.stream(mess_repo.findAll().spliterator(), false)
                .filter(e -> (e.getFrom().getId().equals(logged_user.getId()) || e.getTo().contains(logged_user)) && e.getDate().isAfter(start) && e.getDate().isBefore(end))
                .map(e -> {
                    e.setFrom(repo.findOne(e.getFrom().getId()));
                    return e;
                })
                .collect(Collectors.toList());
    }

    public HashMap<String, Integer> countMessagesPerDate(Long id){
        HashMap<String, Integer> a = new HashMap<>();
        Iterable<Message> messages = getConversation(id);
        messages.forEach(e -> {
            String key = e.getDate().toString().split("T")[0];
            if(a.containsKey(key)){
                a.put(key, a.get(key) + 1);
            }
            else{
                a.put(key, 1);
            }
        });
        return a;
    }

    public Iterable<Message> getConversation(Long id){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        HashMap<String, String> where = new HashMap<>();
        where.put("id1", id.toString());
        where.put("id2", id.toString());
        return StreamSupport.stream(mess_repo.find(where).spliterator(), false)
                .map(e -> {
                    e.setFrom(repo.findOne(e.getFrom().getId()));
                    e.addTo(repo.findOne(e.getTo().get(0).getId()));
                    if(e instanceof ReplyMessage){
                        ((ReplyMessage) e).setReplyTo(mess_repo.findOne(((ReplyMessage) e).getReplyTo().getId()));
                    }
                    return e;
                })
                .sorted((o1, o2) -> {
                    if(o1.getDate().isAfter(o2.getDate())){
                        return 1;
                    }
                    return -1;
                })
                .collect(Collectors.toList());
    }

    public List<Message> send_message(String text, ArrayList<Long> ids, LocalDateTime time){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        ArrayList<Message> rez = new ArrayList<>();
        for(Long id: ids) {
            Message toSend = new Message(text, time);
            toSend.setFrom(logged_user);
            Utilizator recver = repo.findOne(id);
            if(recver == null)
                throw new UserNotExists("User does not exist");
            toSend.addTo(recver);
            mess_repo.save(toSend);
            rez.add(toSend);
        }
        return rez;
    }

    public Iterable<Message> getMessagePage(Long id, int offset, int limit){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        List<Message> a = StreamSupport.stream(((MessageDbRepository) mess_repo).getConversation(id, logged_user.getId(), offset, limit).spliterator(), false)
                .map(e -> {
                e.setFrom(repo.findOne(e.getFrom().getId()));
                e.addTo(repo.findOne(e.getTo().get(0).getId()));
                if(e instanceof ReplyMessage){
                    ((ReplyMessage) e).setReplyTo(mess_repo.findOne(((ReplyMessage) e).getReplyTo().getId()));
                }
                return e;
                })
                .collect(Collectors.toList());
        a.sort((o1, o2) -> {
            if(o1.getDate().isBefore(o2.getDate())) return -1;
            return 1;
        });

        return a;
    }
    public void setObservers(Observer o1, Observer o2){
        ((AbstractDbRepository) mess_repo).remove_observer(o1);
        ((AbstractDbRepository) mess_repo).add_observer(o2);
    }

    public List<Message> sendReplyTo(String text, Long messId, ArrayList<Long> ids, LocalDateTime time){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        ArrayList<Message> rez = new ArrayList<>();
        for(Long id: ids) {
            Message m = mess_repo.findOne(messId);
            ReplyMessage toSend = new ReplyMessage(text, time, m);
            toSend.setFrom(logged_user);
            Utilizator recver = repo.findOne(id);
            if(recver == null)
                throw new UserNotExists("User does not exist");
            toSend.addTo(recver);

            mess_repo.save(toSend);
            rez.add(toSend);
        }
        return rez;
    }

    public Message del_message(Long id){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        return mess_repo.delete(id);
    }

    public void loadPage(Page p) {
        HashMap<Long, Iterable<Message>> map = new HashMap<>();
        p.getFriends().forEach(e -> {
            map.put(e.getId(), getMessagePage(e.getId(),0, 12));
        });
        p.setMessages(map);
        p.setEvents(getEventsPage(0, 12));
        p.setTodayEvents(getSocialEventUserPage(0, 12));
    }

    public Iterable<SocialEvent> getEventsPage(int offset, int limit){

        return StreamSupport.stream(event_repo.find(new HashMap<>(), offset, limit).spliterator(), false)
                .map(e -> {
                    e.setUserAttached(repo.findOne(e.getUserAttached().getId()));
                    return e;
                })
                .collect(Collectors.toList());
    }

    public SocialEvent insertSocialEvent(String title, String desc, LocalDateTime date){
        return event_repo.save(new SocialEvent(title, desc, date, logged_user));
    }

    public void subscribe(Long id){
        Entity<Tuple<Long, Long>> rezid = new Entity<>();
        rezid.setId(new Tuple<>(logged_user.getId(), id));
        usereventsrepo.save(rezid);
    }

    public void unsubscribe(Long id){
        usereventsrepo.delete(new Tuple<>(logged_user.getId(), id));
    }

    public boolean isSubscribed(Long id){
        return usereventsrepo.findOne(new Tuple<>(logged_user.getId(), id)) != null;
    }

    public Iterable<SocialEvent> getSocialEventUserPage(int offset, int limit){
        HashMap<String, String> a = new HashMap<>();
        a.put("userid", logged_user.getId().toString());
        return StreamSupport.stream(usereventsrepo.find(a, offset, limit).spliterator(), false)
                .map(e -> {
                    SocialEvent soc = event_repo.findOne(e.getId().getRight());
                    Utilizator user = repo.findOne(e.getId().getLeft());
                    soc.setUserAttached(user);
                    return soc;
                })
                .filter(e -> Utils.isToday(e.getDate(), LocalDateTime.now()))
                .collect(Collectors.toList());
    }


    public Utilizator isUserRemembered() {
        List<Utilizator> lista = Utils.iterablToList(cachedUser.findAll());
        if(lista.size() == 0){
            return null;
        }
        return lista.get(0);
    }

    public void fastLogin(Utilizator user) {
        logged_user = user;
        update();
    }
}
