package socialnetwork.ui;


import at.favre.lib.crypto.bcrypt.BCrypt;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.encryption.Crypter;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.Page;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.controllers.LoginView;

public class FXUi extends Application{

    Stage shower;
    Scene a;
    UtilizatorService service;
    FriendshipService fr_serv;
    FXMLLoader loader;
    AnchorPane pane;

    @Override
    public void stop() throws Exception {
        if(service.getCurrentUser() != null){
            fr_serv.updateUser();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        Repository<Long, Utilizator> userFileRepository3 =
                new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());
        Repository<Tuple<Long, Long>, Prietenie> friendshipFileRepository3 = new FriendshipDbRepository(url, username, pasword, new PrietenieValidator());
        Repository<Tuple<Utilizator, Utilizator>, FriendRequest> friendreqsDbRepository3 = new FriendRequestsDbRepository(url, username, pasword, new RequestValidator());
        Repository<Long, Message> messageRepository = new MessageDbRepository(url, username, pasword, new MessageValidator());
        Repository<Long, SocialEvent> eventsRepository = new SocialEventsDbRepository(url, username, pasword, new SocialEventValidator());
        Repository<Tuple<Long, Long>, Entity<Tuple<Long, Long>>> usereventsrepo = new UserSocialEventsDbRepository(url, username, pasword, new UserSocialEventValidator());
        Repository<Long, Utilizator> cachedUserRepository = new CachedUserRepository(url, username, pasword, new UtilizatorValidator());

        loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/login_view.fxml"));
        pane = loader.load();
        LoginView login = loader.getController();
        service = new UtilizatorService(userFileRepository3, friendshipFileRepository3, messageRepository, friendreqsDbRepository3, eventsRepository, usereventsrepo,cachedUserRepository, new UserNameValidator());
        fr_serv = new FriendshipService(friendshipFileRepository3,userFileRepository3,friendreqsDbRepository3,cachedUserRepository, new UserNameValidator());
        login.setPage(new Page(service, fr_serv));
        service.add_observer(login);

        a = new Scene(pane, 450, 320);
        shower = new Stage();
        shower.setScene(a);
        login.setStage(shower);
        shower.setTitle("Pied Piper");
        login.initialize();
        shower.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
