package socialnetwork.repository.database;

import socialnetwork.domain.SocialEvent;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.SocialEventValidator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;

public class SocialEventsDbRepository extends AbstractDbRepository<Long, SocialEvent> {


    public SocialEventsDbRepository(String url, String username, String password, SocialEventValidator valid){
        super(url, username, password, valid);
    }

    @Override
    protected SocialEvent mapDbObject(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String title = resultSet.getString("title");
        String description = resultSet.getString("description");
        LocalDateTime date = LocalDateTime.parse(resultSet.getString("date"));
        SocialEvent event = new SocialEvent(id, title, description, date);

        event.setUserAttached(new Utilizator(resultSet.getLong("userid"), null, null));

        return event;
    }

    @Override
    public SocialEvent findOne(Long aLong) {
        String sqlQuerry = "SELECT * from socialevents where id=" + aLong;
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<SocialEvent> find(HashMap<String, String> where, String order, boolean dir) {
        String sqlQuerry = "SELECT * from socialevents";
        setQuery(sqlQuerry);
        return super.find(where, order, dir);
    }

    @Override
    public Iterable<SocialEvent> find(HashMap<String, String> where) {
        String sqlQuerry = "SELECT * from socialevents";
        setQuery(sqlQuerry);
        return super.find(where);
    }

    @Override
    public Iterable<SocialEvent> find(HashMap<String, String> where, int offset, int limit) {
        String sqlQuerry = "SELECT * from socialevents";
        setQuery(sqlQuerry);
        return super.find(where, offset, limit);
    }

    @Override
    public Iterable<SocialEvent> find(HashMap<String, String> where, String order, boolean dir, int offset,  int limit) {
        String sqlQuerry = "SELECT * from socialevents";
        setQuery(sqlQuerry);
        return super.find(where, order, dir, offset, limit);
    }

    @Override
    public Iterable<SocialEvent> findAll() {
        String querry = "SELECT * from socialevents";
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public SocialEvent delete(Long aLong) {
        String querry = "delete from socialevents where id=" + aLong;
        setQuery(querry);
        return super.delete(aLong);
    }

    @Override
    public SocialEvent save(SocialEvent entity) {
        String sqlQuery = "insert into socialevents(title, description, date, userid) values('" + entity.getTitle() + "','" + entity.getDescription() +"','" + entity.getDate() + "'," + entity.getUserAttached().getId() +");";
        setQuery(sqlQuery);
        return super.save(entity);
    }

}
